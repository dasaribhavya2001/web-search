from typing import Any, Text, Dict, List

from hanapin import Google, Bing, DuckDuckGo, Ask
import trafilatura
from transformers import PegasusForConditionalGeneration, PegasusTokenizer
from transformers import pipeline
import torch

from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet

# https://rasa.com/docs/rasa/custom-actions
# https://rasa.com/docs/action-server/sdk-actions


#
# This file is part of a loose module called actions.
#
# Once this domain installed, this module will take the name of the domain.
#
# So don't use `from actions.my_file import stuff` but rather `from .my_file import stuff`.
#


class ValidateSearchForm(FormValidationAction):

    SE_LIST = []

    def __init__(self):
        super().__init__()
        self.SE_LIST = [
            "google",
            "bing",
            "duckduckgo",
            "ask"
        ]

    def name(self) -> Text:
        return "validate_search_form"

    def validate_query(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
        return {'query': slot_value}

    def validate_search_engine(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
        if slot_value.lower() not in self.SE_LIST:
            str_list_se = '\n- '.join(self.SE_LIST)
            msg = f"Implemented engines are:\n- {str_list_se}"
            dispatcher.utter_message(text=msg)
            return {'search_engine': None}
        return {'search_engine': slot_value}

class ActionSubimtSearchWeb(Action):

    def name(self) -> Text:
        return "action_submit_search_web"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        query = tracker.get_slot("query")
        engine = tracker.get_slot("search_engine")

        dispatcher.utter_message(response="utter_submit_search_web", query=query.lower(), engine=engine.capitalize())

        return []

class ActionSummarizeSearchWeb(Action):

    torch_device = None
    summarizer_tokenizer = None
    summarizer_model = None

    nlp = None

    def __init__(self):
        super().__init__()
        
        # Init Pegasus xSum
        summarizer_model_name = "google/pegasus-xsum"
        self.torch_device = "cuda" if torch.cuda.is_available() else "cpu"
        self.summarizer_tokenizer = PegasusTokenizer.from_pretrained(summarizer_model_name)
        self.summarizer_model = PegasusForConditionalGeneration.from_pretrained(summarizer_model_name).to(self.torch_device)

        # Init RoBERTa
        answerer_model_name = "deepset/roberta-base-squad2"
        self.nlp = pipeline('question-answering', model=answerer_model_name, tokenizer=answerer_model_name)

    def name(self) -> Text:
        return "action_search_web_values"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        query = tracker.get_slot("query")
        engine = tracker.get_slot("search_engine")

        answer = None
        
        answer = self.get_webseach_summary(query, engine=engine.lower())

        if not answer:
            dispatcher.utter_message(response="utter_no_data_found", query=query.lower(), engine=engine.capitalize())
        else:
            dispatcher.utter_message(text=answer.capitalize())
        
        return [SlotSet('query', None)] # Return the next state (or listen)

    def get_webseach_summary(self, query, engine="google"):
        # TODO: Use more context
        answer = None
        if len(query) >= 10:
            q = self.get_summary(query)
        else:
            q = query
        # get list of result for query on engine
        result_list = self.get_list_results_with_engine(engine, q)
        
        # Get context
        context = self.get_clean_content_from_url(result_list[0])
        context += "\n" + self.get_clean_content_from_url(result_list[1])
        context += "\n" + self.get_clean_content_from_url(result_list[2])
        
        # Get answer
        answer = self.get_answer_from_context(query, context)
        
        return answer
    
    def get_list_results_with_engine(self, engine, query):
        list_webpages = []
        # Find some relevant links about a seach query
        if engine == "google":
            s = Google(query=query)
        elif engine == "bing":
            s = Bing(query=query)
        elif engine == "duckduckgo":
            s = DuckDuckGo(query=query)
        elif engine == "ask":
            s = Ask(query=query)
        else:
            s = None

        if s:
            list_webpages = [i.get('link') for i in s.results() if i.get('link')]
        return list_webpages
    
    def get_clean_content_from_url(self, content_url):
        # Return the content of a given url
        # TODO: Make more robust against netwok issues
        # TODO: Make more robust agains small pages
        raw_content = trafilatura.fetch_url(content_url)
        if raw_content:
            # Exctract with precision but fast
            content_text = trafilatura.extract(raw_content, favor_precision=True, include_comments=False, include_tables=False, no_fallback=True, target_language='en')
        else:
            print("No raw content in:")
            print(raw_content)
            content_text = ""
        return content_text
    
    def get_summary(self, content: List[Text]):
        # Returns a short summary of a given content
        print("Content")
        print(content)

        model, tokenizer = self.summarizer_model, self.summarizer_tokenizer
        batch = tokenizer(content, truncation=True, padding="longest", return_tensors="pt").to(self.torch_device)
        translated = model.generate(**batch)
        tgt_text = tokenizer.batch_decode(translated, skip_special_tokens=True)

        return tgt_text[0]
    
    def get_answer_from_context(self, question, context):
        # Returns an answer from a question and a piece of context
        # TODO: use confidence score to define a threshold
        print("Summary")
        print(context)
        
        QA_input = {
            'question': question,
            'context': context
        }
        answer = self.nlp(QA_input)
        return answer.get('answer', None)